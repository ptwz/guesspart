/^DEF /{
 delete pins;
 delete packages;
 delete dirs;
 partname=$2;
 package=1;
 in_fplist = 0;
 pincount=0;
}

/^X /{
 pinid=$2;
 pinloc=$3;
 direction=$NF;
 pins[pinloc]=pinid;
 dir[pinloc]=direction;
 pincount++;
}

/^\$FPLIST/{
 in_fplist = 1;
}

!/FPLIST/ && in_fplist {
 packages[package++]=$1;
}

/^\$ENDFPLIST/ {
 in_fplist = 0;
}


(pincount>=3) && /^ENDDEF/{
 #print "INSERT INTO chips ("
 # Filter out 1..2 pin components

 # Output raw import format
 for (l in pins) {
    print partname, l, pins[l], dir[l] > "pins.lst";
 }
 for (i in packages) {
    print partname, packages[i] > "packages.lst";
 }
}
