#!/bin/sh

dbfile=$1
kicad_lib=$2

if [ -z "$dbfile" -o -z "$kicad_lib" ] ; then
    echo Usage
    echo $0 dbfile kicad_lib
    echo ""
    echo Where
    echo    dbfile - Name of sqlite db to be initialized, should not exist
    echo    kicad_lib - Path to kicad library to import
    exit
fi

sqlite3 $dbfile < scripts/schema.sql

# Iterate all file
find $kicad_lib -type f -name "*.lib" -exec cat {} \; | awk -f scripts/lib2sql.awk
# Now pins.lst and packages.lst should be there
if [ ! -s "packages.lst" -o ! -s "pins.lst" ] ; then
    echo "Preparation failed"
fi

sqlite3 $dbfile < scripts/import.sqt
