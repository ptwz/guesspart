CREATE TABLE chip (id, name, dsheet);
CREATE TABLE function (id, name, abstract, dir);
CREATE TABLE import_pin (chipname, pin, function, dir);
CREATE TABLE import_package (chip_name, package_name);
CREATE TABLE abstraction (id, name);
CREATE TABLE sigclass (id, name);
CREATE TABLE classabstract (class, abstraction, propability);
CREATE VIEW chip_pins AS SELECT a.package_name as package_name, b.name as chip_name, count(c.id) as pins  FROM import_package AS a JOIN chip AS b ON a.chip_name=b.name JOIN pin AS c on c.chip=b.id GROUP BY a.package_name, b.name;
CREATE VIEW packages AS SELECT package_name FROM import_package GROUP BY package_name;
CREATE UNIQUE INDEX idx_abstraction_uniq ON abstraction (name);
CREATE UNIQUE INDEX idx_function_uniq ON function (name);
CREATE TABLE function_abstract(
  function,
  abstraction
);
CREATE UNIQUE INDEX idx_mapping_uniq ON function_abstract (function,abstraction);
CREATE UNIQUE INDEX idx_chip_uniq ON chip (name);
CREATE TABLE tmp(
  id,
  chip,
  function,
  location,
  direction
);
CREATE TABLE pin(
  id,
  chip,
  function,
  location,
  direction
);
CREATE UNIQUE INDEX idx_pin_uniq ON pin (chip, location);
CREATE INDEX idx_pin_id ON pin (id);
CREATE INDEX idx_function_id ON function (id);
CREATE UNIQUE INDEX idx_sigclass_name ON sigclass (name);
CREATE TABLE import_class (class,abstract,weight);
CREATE UNIQUE INDEX idx_classabstract_uniq ON classabstract (class, abstraction);
CREATE TABLE foo(
  chip_name,
  pins,
  location,
  package_name
);
CREATE TABLE test(chip_name,pins,location);
