.header on
SELECT 
    chip.name, abstraction.name, function.name, pin.location--, propability, * 
FROM test 
JOIN chip ON test.chip_name=chip.name 
JOIN pin ON pin.location=test.location AND pin.chip=chip.id 
JOIN function ON function.id=pin.function
JOIN function_abstract ON function_abstract.function = pin.function 
JOIN abstraction ON abstraction.id = function_abstract.abstraction
JOIN classabstract ON classabstract.abstraction = function_abstract.abstraction
-- JOIN sigclass ON sigclass.id = classabstract.class
WHERE 
--    test.location='3' 
--    AND
    abstraction.name='UART'
LIMIT 5;
