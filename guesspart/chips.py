from collections import OrderedDict, defaultdict
from guesspart.constraints import packageContraint, pinConstraint
import re

class chips(object):
    '''
    Implements the core idea of chip constraint searching
    and additional helpers like pinmapper that help GUI/TUIs
    display the pin layout in a way you likely encounter in
    the physical world.
    '''
    def __init__(self, conn):
        '''
        Create a new search

        :param conn: An SQLite3 connection
        '''
        self.conn = conn
        self.constraints = {}
        self._cached_constraints = {}
        self.package_constraint = packageContraint(self.conn)
        self.update()
        self.details = None
        self.ranking = None
        self._pinmappers = {".*DIP.*":self._pinmap_dual_inline,
                            ".*SOP.*":self._pinmap_dual_inline}
 

    def _pinmap_stupid(self, pins):
        '''
        Tries to do a halfway descent job at placing the pins on a grid
        for the GUI to ask the user for unknown package types.
        Return the pin ordering as an orderedDict.
        '''
        l = len(pins)
        h = int(l/2)
        
        order = sorted(pins)

        coords = [ (row, col) for row in range(h) for col in range(2) ]
        if len(coords) < l:
            coords.append( (h+1, 1) ) 
        
        return OrderedDict( (x[0],x[1]) for x in zip(coords, order) )

    def _pinmap_dual_inline(self, pins):
        '''
        Try to give a hint to a gui how to place the pins on DIP/SOIC/SSOP 
        packages.
        Return the pin ordering as an orderedDict.
        '''
        l = len(pins)
        h = int(l/2)
        
        order = sorted(pins, key=lambda x:int(x))

        coords = []
        for col in range(2):
            for row in range(h):
                coords.append( (row if col==0 else h-row-1, col))

        if len(coords) < l:
            coords.append( (h+1, 1) ) 
        
        return OrderedDict( (x[0],x[1]) for x in zip(coords, order) )
        

    def update_constraints(self, raw):
        '''
        Sync/import list of constraints.

        Raw constraints are passed as a dict, where
        the key is the pin location as a str, having
        a value that specifies the signal class connected.
        (as in self.get_classes())
        '''
        for pin in raw:
            if pin in self._cached_constraints and self._cached_constraints[pin] == raw[pin]:
                constraint = _cached_constraints[pin]
            else:
                # Constraints have their own caching in place, so no need to
                # do anything here except reuse them
                constraint = pinConstraint(pin, sigclass=raw[pin])
                self._cached_constraints[pin] = constraint
        to_remove = []
        for pin in self._cached_constraints:
            if pin not in raw:
                to_remove.append(pin)

        for pin in to_remove:
            del self._cached_constraints[pin]

        return self.run()

    def run(self):
        '''
        Iterates all constraints and builds a sum of confidences.

        It has several side effects:
        * a detailed pin description of each chip mentioned is built
        * the individual constraints are cached so the need of re-querying
          is reduced

        Returns an ordered dict with chip name as key and confidence as a value
        sorted by summed confidence in descending order, like get_ranking does.
        '''
        sums = defaultdict(float)
        details = defaultdict(list)
        for pin in self._cached_constraints:
            constraint = self._cached_constraints[pin]
            result = constraint.run(self.conn)
            for chip in result:
                sums[chip] += float(result[chip].confidence)
                details[chip].append(result[chip])

        od = OrderedDict()
        for k in sorted(sums, key=lambda x:sums[x]):
            od[k] = sums[k]
        
        self.ranking = od
        self.details = details
        return od

    def get_ranking(self):
        '''
        Returns an ordered dict with chip name as key and confidence as a value
        sorted by summed confidence in descending order. Calls run() if necessary.
        '''
        if self.ranking is None:
            self.run()
        return self.ranking

    def get_details(self):
        '''
        Returns a dict with chip name as key and a list of pin location, function, confidence 
        as a value. Calls run() if necessary.
        '''
        if self.ranking is None:
            self.run()
        return self.details
    
    def _invalidate(self):
        self.ranking = None
        self.details = None

    def update(self):
        self.package_constraint.update()
        self._invalidate()
        self.run()

    def get_packages(self):
        return self.package_constraint.get_packages()

    def get_names(self):
        return self.package_constraint.run()[0]

    def get_pincounts(self):
        return self.package_constraint.run()[1]

    def get_pins(self):
        '''
        Returns the list of pin locations to be asked to the user
        '''
        return self.package_constraint.run()[2]

    def get_pinmap(self, pins):
        '''
        Return the pin names and a grid placement as an orderedDict.
        '''
        mapper = self._pinmap_stupid
        for p in self._pinmappers:
            if not re.match(p, self.package_constraint._name):
                mapper = self._pinmappers[p]
        return mapper(pins)

    def get_classes(self):
        return pinConstraint.get_classes(self.conn)
