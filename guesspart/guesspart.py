import sqlite3
import logging
from guesspart.gui import GUI
from guesspart.constraints import packageContraint, connectionConstraint, pinConstraint
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-d", "--debug",
    help="Enable (lots) of debug output",
    action="store_true")

parser.add_argument("dbfile",
    help="File name the sqlite db to be used",
    action="store",
    default="kicad.sqlite")

args = parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)

sql = sqlite3.connect(args.dbfile)
cur = sql.cursor()
g = GUI(sql)
