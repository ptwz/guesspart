'''
Implements the GUI for guesspart
'''
import tkinter
from tkinter import ttk
from guesspart.chips import chips

class GUI(object):
    def __init__(self, conn):
        self.root = tkinter.Tk()
        self.conn = conn

        frame_all = ttk.Frame(self.root)
        self.chips = chips(conn)

        frame_basics = ttk.Frame(frame_all)
        ttk.Label(frame_basics, text='Package').grid(row=0, column=0)
        self.cb_package_val = tkinter.StringVar()
        self.cb_package = ttk.Combobox(frame_basics, values=["DIP", "SO", "SOIC", "QFP", "BGA", "QFN", "SIL", "TO92"], textvariable=self.cb_package_val)
        self.cb_package.grid(row=0, column=1)
        ttk.Label(frame_basics, text='Pins').grid(row=1, column=0)
        self.cb_package.bind("<<ComboboxSelected>>", self.refresh_ui)

        self.cb_pin_val = tkinter.IntVar()
        self.cb_pin_count = ttk.Combobox(frame_basics, textvariable=self.cb_pin_val)
        self.cb_pin_count.bind("<<ComboboxSelected>>", self.refresh_ui)
        self.cb_pin_count.grid(row=1, column=1)

        #self.notebook.pack()
        #self.query2cb(self.cb_package, "SELECT package_name FROM packages")

        

        self.notebook = ttk.Notebook(frame_all)

        frame_chip = ttk.Frame(self.notebook)
        self.frame_chip = frame_chip

        self.notebook.add(frame_chip, text="Pin signal classes")

        self.frame_pin_crossbar = ttk.Frame(self.notebook)

        self.notebook.add(self.frame_pin_crossbar, text="Pin interconnect")

        frame_results = ttk.Frame(frame_all)
        self.pin_cb_val = {}
        
        self.tv_result = ttk.Treeview(frame_results)
        self.tv_result["columns"] = ("package", "pin", "function", "confidence")
        self.tv_result.heading("package", text="Package")
        self.tv_result.heading("pin", text="Pin #")
        self.tv_result.heading("function", text="Function")
        self.tv_result.heading("confidence", text="Confidence points")
        self.tv_result.pack()

        frame_basics.grid(row=0)
        #frame_chip.grid(row=1)
        #frame_chip.pack()
        self.notebook.grid(row=1)
        frame_results.grid(row=2)
        frame_all.pack()
        self.frame_all = frame_all
        self.result_tbl = None

        self.refresh_ui()
        self.root.mainloop()

    def rebuild_contraints(self, why):
        raw_constraints = { i:self.pin_cb_val[i].get() for i in self.pin_cb_val }

        raw_constraints = { i:raw_constraints[i] for i in raw_constraints if raw_constraints[i]!="" }
        
        self.result_tbl = self.chips.update_constraints(raw_constraints)

        self.refresh_ui(why)

    def show_pins(self, pins):
        # FIXME: Stupid workaround
        #pins = [ x for x in pins if int(x) <= 8]
        l = len(pins)
        half = int(l/2)
        c = 0
        pinmap = self.chips.get_pinmap(pins)
        for k in pinmap:
            (row, column) = k
            val = tkinter.StringVar()
            cb = ttk.Combobox( self.frame_chip, textvariable=val, values=self.chips.get_classes() )
            cb.bind("<<ComboboxSelected>>", self.rebuild_contraints)
            self.pin_cb_val[pinmap[k]] = val
            cb.grid( row=row, column=column*2 )
            lbl = ttk.Label(self.frame_chip, text="Pin {}".format(pinmap[k]))
            lbl.grid( row=row, column=column*2+1 )
            c += 1
        self.frame_all.pack()
        # Now make pin crossbar
        for child in self.frame_pin_crossbar.winfo_children():
            child.destroy()
        row = 0
        crossbar_map = {}

        for p_from in pins:
            col = 0
            for p_to in pins:
                k = frozenset({p_from, p_to})
                if k not in crossbar_map:
                    crossbar_map[k] = tkinter.BooleanVar()
                cb = ttk.Checkbutton(self.frame_pin_crossbar)
                cb.grid( row=row, column=col )
                col += 1
            row += 1
    def refresh_ui(self, why=None):
        tv = self.tv_result
        self.cb_package.config(values=self.chips.get_packages())
        if why == None:
            return
        if why.widget == self.cb_package:
            self.chips.package_constraint.update(name=self.cb_package_val.get(), pins=None)
            self.cb_pin_count.config(values=sorted(list(self.chips.get_pincounts())))
            tv.delete(*tv.get_children())
        elif why.widget == self.cb_pin_count:
            self.chips.package_constraint.update(name=self.cb_package_val.get(), pins=self.cb_pin_val.get())
            self.chips.package_constraint.to_table(self.conn, "test")

            result = self.chips.package_constraint.run()
            tv.delete(*tv.get_children())
            for row in result[2]:
                self.tv_result.insert(parent="", index=99999, text=row[0])
            self.show_pins(result[3])
            self.result_tbl = None
        elif self.result_tbl is not None:
            tv.delete(*tv.get_children())
            details = self.chips.get_details()
            for chip in self.result_tbl:
                key = self.tv_result.insert(parent="", index=0, text=chip, values=("", "", "", self.result_tbl[chip]))
                for row in details[chip]:
                    self.tv_result.insert(parent=key, index=0, text=chip, values=row)

