'''
Implement various constraints to simplfy the querying
'''
from collections import namedtuple
import logging

class constraint(object):
    '''
    Base class for constraints.

    Implement the most basic API all subclasses should have in common.
    '''
    # At which position is this filter supposed to happen.
    # 0 = later, 9 = early
    priority = 0
    # Set true, if only one instance of this class is allowed in a filter chain
    unique = False

    def __init__(self):
        self._query = None
        pass

    def to_table(self, conn, name):
        '''
        (Re)create a temporary table that holds the matching subset of this constraint.

        Used to efficiently chain multiple knock-out criteria without reevaluating all
        possible pins/chips every time a new constraint is processed.
        '''
        drop_query = "DROP TABLE IF EXISTS "+name;
        create_query = "CREATE TABLE "+name+" AS "+self._query
        cur = conn.cursor()
        cur.execute(drop_query)
        res = cur.execute(create_query, self._args)

class packageContraint(constraint):
    '''
    Implements the knock-out constraint which will filter out all
    non matching packages from the bulk.
    '''
    priority = 9
    unique = True
    basic_filters = "%DIP% %SIP% %QFN% %QFP% %SOIC% %SSOP% %TSOP% %SOP% %SIL% %BGA% %TO%92% %TO%223%".split(" ")

    def __init__(self, conn, name=None, pins=None):
        '''
        Creates a new packageContraint.
        Note there can only be one of these per total constraint set,
        as one physical chip in front of you, surely, only has one package you can see.

        For arguments, see self.update
        '''
        self.temp_table = "package_filtered"
        self._package_cache = None
        self._conn = conn
        self.update(name, pins)

    def update(self, name=None, pins=None):
        '''
        Redefines the current chip package constraint.

        :param name: Name of the package type, can contain wildcards or 
                     matching patterns understood by SQLite3's LIKE clause.
                     See `self.get_packages()` for valid values
        :param pins: Number of physical pins the user sees. Can be less that the
                     package has got (like in Quartz-Osciallors that are DIP14,
                     but with only four pins.
        '''
        self._name = name
        self._pins = pins

        self._query = "SELECT chip_name,pins,location FROM chip_pins JOIN chip ON chip_name=chip.name JOIN pin ON  pin.chip==chip.id WHERE 1 "
        self._args = []
        if self._name is not None:
            if '%' in name:
                self._query += " AND package_name LIKE ?"
            else:
                self._query += " AND package_name=?"
            self._args.append(self._name)
        if self._pins is not None:
            self._query += " AND pins=?"
            self._args.append(self._pins)

        self._query += " GROUP BY chip_name,pins,location ORDER BY chip_name"
        self._cache = self._run()

    def get_packages(self):
        '''
        Returns a list of valid packages of generic search terms.
        Mostly intended for GUI use.
        '''
        if self._package_cache is None:
            query = "SELECT package_name FROM packages"
            cur = self._conn.cursor()
            res = cur.execute(query)
            self._package_cache = self.basic_filters + sorted(list(set( [ row[0] for row in res ] ) ))
        return self._package_cache
        
    def run(self):
        '''
        Returns results of packages matching name and (if given) pin count
        constraint.

        :return: Tupe (chips, pins, complete, locations)
            where: 
                - chips is a set of all pin designations seen in 
                  matching parts (VCC,VDD,RXD,..)
                - pins is a set of all pin-counts seen
                - complete is as the name implies a list of whole 
                    (chip_name,pins,location) rows
                - locations a set of all seen pin locations 
        '''
        return self._cache

    def _run(self):
        cur = self._conn.cursor()
        logging.debug("packageContraint: query: |{}|, args |{}|".format(self._query, self._args))
        res = cur.execute(self._query, self._args)
        chips = set()
        pins = set()
        locations = set()
        complete = []
        for row in res:
            logging.debug("packageContraint: result: row |{}|".format(row))
            chips.add(row[0])
            pins.add(row[1])
            locations.add(row[2])
            complete.append(row)
        print(self._query, self._args)
        return (chips, pins, complete, locations)

class connectionConstraint(object):
    '''
    Guesses more information by trying to determine how interconnecting pins
    would make sense.

    From kicad docs:
    I(nput), O(utout), B(idirectional), T(ristate), P(assive), (open) 
    C(ollector), (open) E(mitter), N(on-connected), U(nspecified), or W for 
    power input or w of power output.
    '''

    # Map how much confidence to place in a certain combination
    # of pin functions
    connect_confidence = {
        frozenset(('I', 'I')): 0.5,
        frozenset(('O', 'I')): 0.7,
        frozenset(('O', 'O')): 0.1, # Unlikely, only when paralleling up outputs
        frozenset(('B', 'B')): 0.1,
        frozenset(('B', 'I')): 0.1,
        frozenset(('W', 'I')): 0.4,
        frozenset(('W', 'w')): 0.4,
        frozenset(('w', 'I')): 0.1 
            }

    def __init__(self, pin, sigclass):
        self.pin = pin
        self.sigclass = str(sigclass)
        self.cache = None

class pinConstraint(object):
    unique = False
    _result_row = namedtuple("_result_row", ("chip","location","function","confidence"))
    raw_query = """
    SELECT 
        chip.name, test.location, function.name, propability
    FROM test 
    JOIN chip ON test.chip_name=chip.name 
    JOIN pin ON pin.location=test.location AND pin.chip=chip.id 
    JOIN function ON function.id=pin.function
    JOIN function_abstract ON function_abstract.function = pin.function 
    JOIN abstraction ON abstraction.id = function_abstract.abstraction
    JOIN classabstract ON classabstract.abstraction = function_abstract.abstraction
    JOIN sigclass ON sigclass.id = classabstract.class
    WHERE 
        test.location=?
    AND
        sigclass.name=?
    """

    @classmethod
    def get_classes(cls, conn):
        query = "SELECT name FROM sigclass"
        cur = conn.cursor()
        res = cur.execute(query)
        return [ x[0] for x in res.fetchall() ]

    def __init__(self, pin, sigclass):
        self.pin = pin
        self.sigclass = str(sigclass)
        self.cache = None

    def run(self, conn):
        '''
        Returns a dict of matching chip names and their respective _result_row
        named tuples
        '''
        if self.cache is None:
            logging.debug("pinConstraint: query: |{}|, args |{}|".format(self.raw_query, [self.pin, self.sigclass]))
            cur = conn.cursor()
            res = cur.execute(self.raw_query, [self.pin, self.sigclass])
            ret = {}
            for row in res:
                logging.debug("pinConstraint: result: row |{}|".format(row))
                ret[row[0]] = self._result_row(*row)
            cache = ret
        else:
            ret = cache
        return ret

