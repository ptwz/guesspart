from distutils.core import setup
import setuptools

setup(
    name = "guesspart",
    version = '0.1',
    description = 'A (graphical) tool to help guessing an unkown chip from footprint or signals attached.',
    url = 'https://gitlab.com/ptwz/guesspart',
    author = 'Peter Turczak',
    license = 'MIT',
    packages = ["guesspart"],
    python_requires = '>=3.5',
    scripts = ['GuessPart']
)
