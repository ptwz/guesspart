# GuessPart, a reverse engineering aid for electronics

This project aims at people that revese engineer circuit boards either for fun or profit (i.e. people who do security research, the repair community, etc). It is supposed to assist the user in identifying integrated circuits. Normally this is a no-brainer task of just reading the part number and applying some internet searching skills.

But somtimes things are a littel more nasty; there are three exemplary use cases to describe the aim of the project:

1. Unfortunately there sometimes vendors of highly priced equipment relabel the chips to better suit their internal part numbers. Some might even say to this would be to force people to buy overprices jelly-bean parts, which nobody does, right?

2. Sometimes mid to low priced products have the part numbers of certain core components ground/milled off, maybe for "security" or other business reasons.

3. Even worse, sometimes a component failed fatally, leaving only scorched remains that keeps the average repair tech in the dark. These somoldered remains of an IC more often than not burn away their valuable part numbers.

In all these above cases, this tool might come im handy.

## How to use it?
There are three steps to identify a part

1. Specify the package, like DIP (Dual inline package), SOIC (Small Outline Integated Cicuit), ...

2. Select the number of pins you see, this might be different from their numbers. You might say: What? Wait, you are telling me there are 8 pin DIP parts that are not DIP-8?! Yes there are, consider some optocouplers, they have only eight pins, but may be streched, so that their pin numbers in classical DIP count may end up in 1, 2, 6, 7, 13, 14.
```
     +---\_/----+
    #| 1     14 |#
    #| 2     13 |#
     | 3     12 |
     | 4     11 |
     | 5     10 |
    #| 6      9 |#
    #| 7      8 |#
     +----------+
```

3. Give as much input to the program as you can. Try and guess or buzz out as may connections as you can. Once you find supply lines, maybe quarz oscillators, SPI- or I2C-busses you provide these signal class guesses to the program. It will try and figure out the best match from the database and give you a list of more or less likely candidate chips for the given package and pin connections.

## How does it work?
This tool imports the great and ever evolving KiCAD part database. It groups their pin designations into certain categories. These in turn are then mapped to a likelyhood of a certain functional network being attached to them. This sums up over a chip to yield a likelyhood of the chip matching the given constraints.

## How to install it?
In order to use this tool, you need to have Python3 installed and convert the KiCAD database. This can be done as follows (applies Linux/MacOS, Windows is not yet tested):

1. Get a reasonably new version of the following programs
    * python (development happens on 3.5.3)
    * sqlite3 command line tool
    * git
2. First checkout this tool: `git clone --recurse-submodules https://gitlab.com/ptwz/guesspart.git`
3. Create the database: `cd guesspart ; sh scripts/init.sh kicad.sqlite kicad-symbols-master`
4. Install the tool `python3 setup.py install`
5. Run it: `python3 GuessPart` (Running `GuessPart` does not work yet, have to figure out, why)


